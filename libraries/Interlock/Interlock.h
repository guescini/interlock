/*
Interlock.h - All the stuff needed for the ATLAS SCT Upgrade interlock unit

Created Bart Hommels 3/7/2012
*/

#ifndef Interlock_h
#define Interlock_h

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif





class NTCtemp
{
 public:
  void setNTCup(int _inPin, int _nSamples, double _Rseries);
  void setNTCParameters(double _Rref, double _Tref, double _B);
  double getDegC();

 private:
  // input pin
  int inPin;
  // NTC series resistance
  double Rseries;
  // NTC parameters, reference (R, T)
  double Rref, Tref, Bntc;
  //
  int nSamples;
  // variables used for calculation
  double sumNTC, avgNTC, Rntc, TempK;

};
#endif
