/*
Interlock.h - All the stuff needed for the ATLAS SCT Upgrade interlock unit

Created Bart Hommels 3/7/2012
*/


#include "NTCreadout.h"
#include <math.h>


//-----------------------------------------------------------------
// setup the NTC pin, nSamples for averaging, series Resistor used
//-----------------------------------------------------------------
void NTCtemp::setNTCup(int _inPin, int _nSamples, double _Rseries)
{
  inPin = _inPin;
  nSamples = _nSamples;
  Rseries = _Rseries;
}

//-----------------------------------------------------------------
// set the NTC specific values and (R,T) reference value
//-----------------------------------------------------------------
void NTCtemp::setNTCParameters(double _Rref, double _Tref, double _B)
{
  Rref = _Rref;
  Tref = _Tref;
  Bntc = _B;
}

//-----------------------------------------------------------------
// return the calculated temperature in deg C for the NTC
// ADC readings are averaged for nAvg for smoothening
//-----------------------------------------------------------------

double NTCtemp::getDegC()
{
  sumNTC = 0.0;
  // read ADC values and average
  for (int iSamples = 0; iSamples < nSamples; iSamples++){
    sumNTC += analogRead(inPin);
  }
  avgNTC = sumNTC/double(nSamples);

  // calculate Rntc in voltage divider:
  Rntc = Rseries/((1024.0/avgNTC)-1);
  // Steinhart-Hart for extracting temperature in deg K:
  TempK = 1/((1/Tref)+((1/Bntc)*log(Rntc/Rref)));
  // return temperature in deg C
  return(TempK-273.15);
}




