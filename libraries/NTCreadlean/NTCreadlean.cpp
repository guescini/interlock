/*
NTC.cpp - Class for extracting temperatures in Celsius
from input readings from the Arduino ADCs

It assumes the NTC being connected to Vcc through a series resistor on one end, and
to ground at the other end

Created Bart Hommels 13/2/2012
modified to get rid of doubles 16/7/2012

*/

//#include "Wprogram.h"
#include "NTCreadlean.h"
#include <math.h>


//-----------------------------------------------------------------
// setup the NTC pin, nSamples for averaging, series Resistor used
//-----------------------------------------------------------------
void NTCtemp::setNTCup(byte _inPin, byte _nSamples, byte _Rseries)
{
  inPin = _inPin;
  nSamples = _nSamples;
  Rseries = _Rseries;
}

//-----------------------------------------------------------------
// set the NTC specific values and (R,T) reference value
//-----------------------------------------------------------------
void NTCtemp::setNTCParameters(byte _Rref, byte _Tref, int _B)
{
  Rref = _Rref;
  Tref = _Tref;
  Bntc = _B;
}

//-----------------------------------------------------------------
// return the calculated temperature in deg C for the NTC
// ADC readings are averaged for nAvg for smoothening
//-----------------------------------------------------------------

float NTCtemp::getDegC()
{
  int sumNTC = 0;
  // read ADC values and average
  for (byte iSamples = 0; iSamples < nSamples; iSamples++){
    sumNTC += analogRead(inPin);
  }
  // calculate Rntc in voltage divider:
  float Rntc = (1000.0*(float)Rseries)/((1024.0/( (float)sumNTC/float(nSamples) ) )-1);
  // Steinhart-Hart for extracting temperature in deg K:
  // return temperature in deg C
  return( (1/((1/((float)Tref+273.15))+((1/(float)Bntc)*log(Rntc/(1000.0*(float)Rref)))))-273.15 );
}




