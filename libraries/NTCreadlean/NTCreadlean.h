/*
NTC.h - Class for extracting temperatures in Celsius
from input readings from the Arduino ADCs

It assumes the NTC being connected to Vcc through a series resistor on one end, and
to ground at the other end

Created Bart Hommels 13/2/2012
modified to get rid of doubles 16/7/2012
*/

#ifndef NTCreadlean_h
#define NTCreadlean_h

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif


class NTCtemp
{
 public:
  void setNTCup(byte _inPin, byte nSamples, byte _Rseries);
  void setNTCParameters(byte _Rref, byte _Tref, int _B);
float getDegC();

 private:
  // input pin
 byte inPin, nSamples, Rseries;
  // NTC series resistance in discrete kOhm steps
  // NTC parameters, reference (R, T)
 byte Rref, Tref;
  int Bntc;

};
#endif
