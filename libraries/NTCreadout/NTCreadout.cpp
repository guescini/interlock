/*
NTC.cpp - Class for extracting temperatures in Celsius
from input readings from the Arduino ADCs

It assumes the NTC being connected to Vcc through a series resistor on one end, and
to ground at the other end

Created Bart Hommels 13/2/2012
modified to get rid of doubles 16/7/2012

*/

//#include "Wprogram.h"
#include "NTCreadout.h"
#include <math.h>


//-----------------------------------------------------------------
// setup the NTC pin, nSamples for averaging, series Resistor used
//-----------------------------------------------------------------
void NTCtemp::setNTCup(int _inPin, int _nSamples, float _Rseries)
{
  inPin = _inPin;
  nSamples = _nSamples;
  Rseries = _Rseries;
}

//-----------------------------------------------------------------
// set the NTC specific values and (R,T) reference value
//-----------------------------------------------------------------
void NTCtemp::setNTCParameters(float _Rref, float _Tref, float _B)
{
  Rref = _Rref;
  Tref = _Tref;
  Bntc = _B;
}

//-----------------------------------------------------------------
// return the calculated temperature in deg C for the NTC
// ADC readings are averaged for nAvg for smoothening
//-----------------------------------------------------------------

float NTCtemp::getDegC()
{
  sumNTC = 0.0;
  // read ADC values and average
  for (int iSamples = 0; iSamples < nSamples; iSamples++){
    sumNTC += analogRead(inPin);
  }
  avgNTC = sumNTC/float(nSamples);

  // calculate Rntc in voltage divider:
  Rntc = Rseries/((1024.0/avgNTC)-1);
  // Steinhart-Hart for extracting temperature in deg K:
  TempK = 1/((1/Tref)+((1/Bntc)*log(Rntc/Rref)));
  // return temperature in deg C
  return(TempK-273.15);
}




