/*
NTC.h - Class for extracting temperatures in Celsius
from input readings from the Arduino ADCs

It assumes the NTC being connected to Vcc through a series resistor on one end, and
to ground at the other end

Created Bart Hommels 13/2/2012
modified to get rid of doubles 16/7/2012
*/

#ifndef NTCreadout_h
#define NTCreadout_h

#if ARDUINO >= 100
 #include "Arduino.h"
#else
 #include "WProgram.h"
#endif


class NTCtemp
{
 public:
  void setNTCup(int _inPin, int nSamples, float _Rseries);
  void setNTCParameters(float _Rref, float _Tref, float _B);
float getDegC();

 private:
  // input pin
  int inPin;
  // NTC series resistance
  float Rseries;
  // NTC parameters, reference (R, T)
  float Rref, Tref, Bntc;
  //
  int nSamples;
  // variables used for calculation
  float sumNTC, avgNTC, Rntc, TempK;

};
#endif
